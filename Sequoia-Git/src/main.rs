use std::env;
mod verify;
mod sign;
mod messages;
use std::fs::File;
use std::io::prelude::*;

/*TODO
    - Add help page
    - Help page should appear when being given an unknown command (error report as well perhaps)
    - Given no extra flags, handle exception
    - See if parsing commands can be done with CLAP library (readability/Sequoia-PGP simmilarity)
    */


fn main() {
    let args: Vec<String> = env::args().collect();
    println!("{:?}\n\n\n",args);
    //let mut file = File::create("foo.txt");
    //file.unwrap().write_all(args[2].as_bytes());
    //get first arg which isn't the binary itself
    if args.len() > 1 {
    
    let first_flag = &args[2];

    if first_flag == "--verify"{
        if args.len() == 5 {
            let signature = &args[2];
            let file = &args[4];
            verify::verify_handler(signature,file);
        }
        
        else{
            //TODO kill proc
            println!("interface did not receive enough arguments to perform '--verify' ");

        }
    }
    else if first_flag == "-bsau" {
        //let mut file = File::create("debug.txt");
        //file.unwrap().write_all(args.len().to_string().as_bytes());
        if args.len() == 4 {
            let key = &args[3];
            sign::sign_handler(key);
        }
    }
    else{
        println!("reroute proc");
    }
    }
    else {
        messages::help_message()
    }
}
