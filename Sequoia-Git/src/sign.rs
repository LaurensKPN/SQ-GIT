use std::io::{self,Write,Read};
use std::convert::TryInto;
use std::fs::File;
use std::io::prelude::*;
//some redundant imports included
extern crate sequoia_openpgp as openpgp;
use openpgp::cert::prelude::*;
use openpgp::serialize::stream::*;
use openpgp::packet::prelude::*;
use openpgp::parse::stream::*;
use openpgp::policy::Policy;
use openpgp::policy::StandardPolicy;


//TODO make cases where non-OK results appear.
//TODO handle non-OK results
pub fn sign_handler(key: &std::string::String) -> std::io::Result<()>{
    
    let unsigned_input = stdin_reader();
    let pol = &StandardPolicy::new();
    let signing_key = generate_cert().unwrap();
    let mut signed_message = Vec::new();
    // signed contents are being written to signed_message
    sign(pol,&mut signed_message, &unsigned_input.unwrap(), &signing_key).unwrap();
    //print signed content to stdout
    print!("{:?}",&signed_message[..]);
    //let mut file = File::create("signed.txt")?;
    //file.write_all(&signed_message[..]);
    Ok(())
}
// Reads stdin to get the commit
fn stdin_reader() -> std::io::Result<String>{
    let mut buffer = String::new();
    io::stdin().read_to_string(&mut buffer);
    io::stdin().lock();
    //let mut file = File::create("reader.txt")?;
    //file.write_all(buffer.as_bytes());
    println!("{}", buffer);
    
    Ok(buffer)   
} 

fn second_reader() -> std::io::Result<String>{
    let mut buffer = String::new();
    //let mut file = File::create("reader.txt")?;
    match io::stdin().read_line(&mut buffer) {
        Ok(n) => {
            println!("{} bytes read" , n);
            println!("{}", buffer);
            
            //file.write_all(buffer.as_bytes());
            Ok(buffer)
        }
        Err(error) => {
            //file.write_all(error.to_string().as_bytes());
            Ok(error.to_string())
        },
    }
    
}
//TODO write stdin_writer
fn gnupg_message_sender(){
    let response_array = ["\n[GNUPG:] SIG_CREATED ","\n[GNUPG:] GOODSIG "];
    //print should write to stdout.
    print!("{}",response_array[0]);
    
}

//Sequoia keygen fn
fn generate_cert() -> openpgp::Result<openpgp::Cert> {
    let (cert, _revocation) = CertBuilder::new()
        .add_userid("USERID")
        .add_signing_subkey()
        .generate()?;
    Ok(cert)
}

//Sequoia sign function
fn sign(policy: &dyn Policy, sink: &mut Write, plaintext: &str, tsk: &openpgp::Cert)-> openpgp::Result<()>
    {
    // Get the keypair to do the signing from the Cert.
    let keypair = tsk
        .keys().unencrypted_secret()
        .with_policy(policy, None).alive().revoked(false).for_signing()
        .nth(0).unwrap().key().clone().into_keypair()?;

    // Start streaming an OpenPGP message.
    let message = Message::new(sink);

    // We want to sign a literal data packet.
    let signer = Signer::new(message, keypair).build()?;

    // Emit a literal data packet.
    let mut literal_writer = LiteralWriter::new(signer).build()?;

    // Sign the data.
    literal_writer.write_all(plaintext.as_bytes())?;

    // Finalize the OpenPGP message to make sure that all data is
    // written.
    literal_writer.finalize()?;

    Ok(())
    }


    
//git-scm.com/docs/signature-format