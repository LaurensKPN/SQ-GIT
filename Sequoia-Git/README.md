# Sequoia-PGP Git interface
Signing & verification of Git commits using the [Sequoia-PGP library](https://gitlab.com/sequoia-pgp/sequoia).

__In its current state, this project can not yet be used.__
## Coupling with git
To make Git call this interface, please perform the following steps:
1. `cargo run` this repository. 
2. add `./Sequoia-Git/target/debug/sqg` to PATH.
3. edit your Git config by calling `git config --global --edit`
4. Add: `[gpg] program = sqg` to the config file.

Now every time that you sign a commit with -S, SQG will sign it.

